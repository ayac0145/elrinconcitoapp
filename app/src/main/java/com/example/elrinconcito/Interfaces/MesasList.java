package com.example.elrinconcito.Interfaces;

public class MesasList {
    public String mesa;
    public String pedido;
    public String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public MesasList(String mesa, String pedido, String color) {
        this.mesa = mesa;
        this.pedido = pedido;
        this.color = color;
    }

    public String getMesa() {
        return mesa;
    }

    public void setMesa(String mesa) {
        this.mesa = mesa;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }
}
