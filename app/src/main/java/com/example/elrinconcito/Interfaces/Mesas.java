package com.example.elrinconcito.Interfaces;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.elrinconcito.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Mesas extends AppCompatActivity {
    List<MesasList> elements  = new ArrayList<>();
    MesasList msList;
    FloatingActionButton fab;
    com.getbase.floatingactionbutton.FloatingActionButton fab1,fab2,fab3,fab4,fab5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mesas);
        init();
        fab1 = findViewById(R.id.fab_action1);
        fab2 = findViewById(R.id.fab_action2);
        fab3 = findViewById(R.id.fab_action3);
        fab4 = findViewById(R.id.fab_action4);
        fab5 = findViewById(R.id.fab_action5);

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                añadir("Mesa 1");
                fab1.setEnabled(false);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                añadir("Mesa 2");
                fab2.setEnabled(false);
            }
        });
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                añadir("Mesa 3");
                fab3.setEnabled(false);
            }
        });
        fab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                añadir("Mesa 4");
                fab4.setEnabled(false);
            }
        });
        fab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                añadir("Mesa 5");
                fab5.setEnabled(false);
            }
        });

    }
    public void init(){
        MesasListAdapter listAdapter = new MesasListAdapter(elements,this);
        RecyclerView recyclerView = findViewById(R.id.ListRV);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }
    public void añadir(String mesa){
        elements.add(new MesasList(mesa,"Pedido n", "#f52459"));
        init();
    }
}
