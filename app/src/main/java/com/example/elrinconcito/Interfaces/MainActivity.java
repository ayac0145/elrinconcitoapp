package com.example.elrinconcito.Interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.elrinconcito.Controladores.AccionesSQL;
import com.example.elrinconcito.R;
import com.example.elrinconcito.Controladores.ConexionSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnLog; //Variable tipoBoton
    private EditText txtNom, txtPass; //Variables tipo EditText
    private AccionesSQL aSQL = new AccionesSQL(); //Se crea una variable de AccionesSQL para poder usar sus metodos
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLog = findViewById(R.id.btnIngresar);
        txtNom = findViewById(R.id.txtUser);
        txtPass= findViewById(R.id.txtPassword);
        findViewById(R.id.btnIngresar).setOnClickListener(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnIngresar: //Comprobamos que el envento sea del boton para ingresar

                 if (validaCampos()){
                    ResultSet rsLog; //Se crea una variable tipo ResultSet para almacenar la informacion
                    try {
                        rsLog = aSQL.ConsultaUser(txtNom.getText().toString(),txtPass.getText().toString()); //a rsLog se le asigna el Resultset de la clase ConsultaUser
                        //Que pertenece a AccionesSQL
                        if(rsLog.next()){ //Si existe un registro con los mismos datos de los Input ....
                            System.out.println("El usuario Existe!!");
                            Toast.makeText(getApplicationContext(), "BIENVENIDO!", Toast.LENGTH_LONG).show();
                            //Una vez que se ejecuta el Resultset se cierra la conexion por si mismo, por lo que no hay necesidad de cerrarla
                            Intent i = new Intent(getApplicationContext(),Mesas.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(getApplicationContext(), "USUARIO O CONTRASEÑA INCORRECTOS", Toast.LENGTH_LONG).show();
                        }
                    } catch (SQLException e){
                        e.printStackTrace();
                    }
                }

                 /* Connection conn = connSQL.ConexionBD();
                    String query="SELECT *FROM Usuarios WHERE nombre = ? and password = ?" ;
                    try {
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.setString(1,txtNom.getText().toString());
                        pst.setString(2,txtPass.getText().toString());
                        System.out.println(pst);

                        ResultSet rst = pst.executeQuery();
                        if(rst.next()){
                            System.out.println("El usuario Existe!!");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }*/

                break;
        }

    }
    public boolean validaCampos(){
        if(txtNom.getText().toString().matches("") && txtPass.toString().matches("")){
            Toast.makeText(getApplicationContext(), "INTRODUCE UN USUARIO Y UNA CONTRASÑEA", Toast.LENGTH_LONG).show();
            return false;
        }if(txtNom.getText().toString().matches("")){
            Toast.makeText(getApplicationContext(), "INTRODUCE UN USUARIO", Toast.LENGTH_LONG).show();
            return false;
        }if(txtPass.getText().toString().matches("")){
            Toast.makeText(getApplicationContext(), "INTRODUCE UNA CONTRASEÑA", Toast.LENGTH_LONG).show();
            return false;
        }else{return true;}

    }
}
