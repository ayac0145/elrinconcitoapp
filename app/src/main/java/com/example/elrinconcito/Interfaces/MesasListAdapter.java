package com.example.elrinconcito.Interfaces;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.elrinconcito.R;

import java.util.List;

public class MesasListAdapter extends RecyclerView.Adapter<MesasListAdapter.ViewHolder> {
    private List<MesasList> mData;
    private LayoutInflater mInflater;
    private Context context;

    public MesasListAdapter(List<MesasList> itemList, Context context){
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData=itemList;
    }

    @Override
    public int getItemCount(){return mData.size();}
    @Override
    public MesasListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = mInflater.inflate(R.layout.lista_mesas,null);
        return new MesasListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final MesasListAdapter.ViewHolder holder, final int position){
        holder.binData(mData.get(position));
    }
    public void setItems(List<MesasList> items){mData=items;}
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iconImage;
        TextView mesa, pedido;
        ViewHolder(View itemView){
            super(itemView);
            iconImage=itemView.findViewById(R.id.icoImageView);
            mesa = itemView.findViewById(R.id.txtVwMesas);
            pedido = itemView.findViewById(R.id.txtVwPedido);

        }
        void binData(final MesasList item){
            iconImage.setColorFilter(Color.parseColor(item.getColor()), PorterDuff.Mode.SRC_IN);
            mesa.setText(item.getMesa());
            pedido.setText(item.getPedido());
        }
    }
}
