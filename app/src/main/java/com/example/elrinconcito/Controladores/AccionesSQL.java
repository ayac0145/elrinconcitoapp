package com.example.elrinconcito.Controladores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AccionesSQL {
    public ResultSet ConsultaUser(String nom, String pass) throws SQLException{
        String query="SELECT *FROM Usuarios WHERE NOMBRE = ? and password=?" ;
        ConexionSQL conn = new ConexionSQL();
        PreparedStatement pst = conn.ConexionBD().prepareStatement(query);
        pst.setString(1,nom);
        pst.setString(2,pass);
        ResultSet rs = pst.executeQuery();
        return rs;
    }
}
