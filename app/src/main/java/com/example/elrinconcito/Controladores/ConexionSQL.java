package com.example.elrinconcito.Controladores;

import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionSQL {

    public Connection ConexionBD() {
        Connection conn = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            String urlCon= "jdbc:jtds:sqlserver://equipo3.database.windows.net;databaseName=pruebas;user=Crepas;password=Elrinconcito123";
            conn = DriverManager.getConnection(urlCon);
            System.err.println("Conexion EXITOSA");
        } catch (SQLException e) {
            System.out.println("ERROR SQL "+ e );
        }catch (Exception ex){
            System.out.println("OTRO ERROR");
        }
        return conn;
    }
    /*public void CierraConexionBD(){
        try{
            conn.close();
        }catch (SQLException sqle){
            System.out.println(sqle);
        }
    }*/
}

